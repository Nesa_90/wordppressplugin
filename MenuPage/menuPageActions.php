<?php

use ProductDiscount\Mapper\ProductDiscount as Mapper;
use ProductDiscount\Repository\ProductDiscount as Repository;

$action = $_GET['action'] ?? 'view';
$mapper = new Mapper();
$productDiscountRepo = new Repository($mapper);
$page = $_GET['paginationPage'] ?? 1;
switch ($action) {
    case 'view':
        $items = $productDiscountRepo->getAll($page, 20);
        include DISCOUNT_DIR . 'templates/discountList.php';
        break;
    case 'createForm':
        $product = wc_get_product($_GET['productId']);
        $formAction = '?page=discount-manager&action=create';
        if (isset($_GET['discountId'])) {
            $discountData = $productDiscountRepo->getDiscountById((int)$_GET['discountId']);
            $productDiscountId = $discountData->getId();
            $dateStart = $discountData->getDateStart('yy-m-d');
            $dateEnd = $discountData->getDateEnd('yy-m-d');
            $productId = $discountData->getProductId();
            $quantityStep = $discountData->getQuantityStep();
            $discountPercentage = $discountData->getDiscountPercentage();
            $discountValue = $discountData->getDiscountValue();
            $minEndPrice = $discountData->getMinEndPrice();
            $formAction = '?page=discount-manager&action=update&discountId=' . $productDiscountId . '&productId=' . $productId;
        }
        include DISCOUNT_DIR . 'templates/discountCreateForm.php';
        break;
    case 'create':
        $data = ProductDiscount\Service\PostFormatter::formatDataFromForm($_POST);
        $id = $productDiscountRepo->create($data);
        if ($id) {
            $dateNow = new DateTime();
            if ((isset($data['dateStart']) && $data['dateStart'] > $dateNow->getTimestamp()) && !wp_next_scheduled('productDiscountScheduleSale',
                    ['discountId' => $id])) {
                $productDiscountRepo->changeDiscountToInactive($id);
                wp_schedule_single_event($data['dateStart'], 'productDiscountScheduleSale', ['discountId' => $id]);
            }
            if ((isset($data['dateEnd'])) && !wp_next_scheduled('productDiscountDisableSale', ['discountId' => $id])) {
                wp_schedule_single_event($data['dateEnd'], 'productDiscountDisableSale', ['discountId' => $id]);
            }
            echo '<p>Uspešno ste snimili popust</p>';
        } else {
            echo '<p>Doslo je dogreške pri snimanju popusta</p>';
        }

        break;
    case 'update':
        $data = ProductDiscount\Service\PostFormatter::formatDataFromForm($_POST);
        $data['productDiscountId'] = (int)$_GET['discountId'];
        $productDiscountRepo->update($data);
        echo '<p>Uspešno ste izmenili popust</p>';
        break;
    case 'delete':
        if (isset($_GET['productDiscountId'])) {
            $productDiscountRepo->delete((int)$_GET['productDiscountId']);
        }
        echo '<p>Uspešno ste obrisali popust</p>';
        break;
}