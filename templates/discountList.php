<div class="wrap">
    <h2>Lista aktivnih Popusta</h2>
    <table id="discountList">
        <thead>
        <tr>
            <th>Proizvod</th>
            <th>Uslov</th>
            <th>Procenat popusta</th>
            <th>Vrednost popusta</th>
            <th>Minimalna cena proizvoda</th>
            <th>Kolicina prodatih proizvoda</th>
            <th>Pocetak Akcije</th>
            <th>Kraj Akcije</th>
            <th>Ackije</th>
        </tr>
        </thead>
        <tbody>
        <?php
        /** @var ProductDiscount\Model\ProductDiscount $item */
        foreach ($items as $item):?>
            <tr>
                <td>
                    <a href="<?=admin_url() . '?page=discount-manager&action=createForm&discountId=' .
                    $item->getId() . '&productId=' . $item->getProductId()?>">
                        <?=get_the_title($item->getProductId())?>
                    </a>
                </td>
                <td><?=$item->getQuantityStep()?></td>
                <td><?=$item->getDiscountPercentage() . '%'?></td>
                <td><?=$item->getDiscountValue() . 'RSD'?></td>
                <td><?=$item->getMinEndPrice() . 'RSD'?></td>
                <td><?=$item->getQuantitySold()?></td>
                <td>
                    <?php
                    echo $item->getDateStart('d/m/yy');
                    ?>
                </td>
                <td>
                    <?php
                    if ($item->getDateEnd() !== null) {
                        echo $item->getDateEnd('d/m/yy');
                    } else {
                        echo 'Istek zaliha';
                    }
                    ?>
                </td>
                <td>
                    <a href="<?=admin_url() . '?page=discount-manager&action=delete&productDiscountId=' . $item->getId()?>">
                        Obriši
                    </a>
                </td>
            </tr>
        <?php
        endforeach; ?>
        </tbody>
    </table>
    <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            jQuery('#discountList').DataTable();
        });
    </script>
</div>