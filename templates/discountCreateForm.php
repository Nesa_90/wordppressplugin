<div class="wrap">
<h2><?=$product->get_name()?></h2>
<div class="productPreview">
    <?=$product->get_image()?>
</div><span id="pricePreview"
            data-price="<?=$product->get_price()?>">Cena Proizvoda:<?=$product->get_price_html()?></span>
<form id="discountForm" method="post" action="<?=$formAction?>">
    <div>
        <label for="quantityStep">Unesite na koliko prodatih proizvoda se smanjuje cena</label>
        <input type="number" id="quantityStep" name="quantityStep" value="<?=$quantityStep ?? '1'?>">
    </div>
    <div>
        <label for="discountPercentage">Popust u %</label>
        <input type="number" step="any"  id="discountPercentage" name="discountPercentage" value="<?=$discountPercentage ?? ''?>">
    </div>
    <div>
        <label for="discountValue">Popust u din</label>
        <input type="number" step="any" id="discountValue" name="discountValue" value="<?=$discountValue ?? ''?>">
    </div>
    <div>
        <label for="minPrice">Minimalna cena proizvoda</label>
        <input type="number" step="any" id="minPrice" name="minEndPrice" value="<?=$minEndPrice ?? ''?>">
    </div>
    <div>
        <label for="actionStart">Početak akcije:</label>
        <input type="date" id="actionStart" name="dateStart" value="<?= $dateStart ?? ''?>">
    </div>
    <div>
        <label for="actionEnd">Kraj akcije:</label>
        <input type="date" id="actionEnd" name="dateEnd" value="<?=$dateEnd ?? ''?>">
    </div>
    <div>
        <input type="hidden" value="<?=$product->get_id()?>" name="productId">
        <input id="submit" type="submit" value="Submit">
    </div>
</form>
</div>