<?php


namespace DiscountManager\Setup;


use DiscountManager\MenuPage\MenuPage;
use ProductDiscount\Service\Order;
use ProductDiscount\Service\SaleScheduler;

class Setup
{
    /**
     * @var MenuPage
     */
    private $adminPage;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var SaleScheduler
     */
    private $saleScheduler;

    public function __construct(MenuPage $menuPage, Order $order, SaleScheduler $saleScheduler)
    {
        $this->adminPage = $menuPage;
        $this->order = $order;
        $this->saleScheduler = $saleScheduler;
    }

    public function setup(): void
    {
        $this->hooksAndFilters();
    }

    private function hooksAndFilters(): void
    {
        add_filter('manage_edit-product_columns', [$this, 'addNewColumnToProduct'], 99, 2);
        add_filter('manage_posts_custom_column', [$this, 'populateCustomColumn'], 10, 2);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminCss']);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminJs']);
        $this->saleScheduler->hooksAndFilters();
        $this->order->hooksAndFilters();
    }

    public function addNewColumnToProduct($columns): array
    {
        $firstThreeColumns = array_slice($columns, 0, 3);
        $restOfTheColumns = array_slice($columns, 3);
        $discountColumn = ['discountManager' => 'Discount Manager'];
        return array_merge($firstThreeColumns, $discountColumn, $restOfTheColumns);
    }

    public function populateCustomColumn($columnName, $productId): void
    {
        if ($columnName === 'discountManager'):?>
            <a href="<?=menu_page_url($this->adminPage->getMenuSlug(),
                false) . '&action=createForm&productId=' . $productId?>">
                Dodajte popust</a>
        <?php
        endif;
    }

    public function enqueueAdminCss(): void
    {
        wp_enqueue_style('discountAdminCss', DISCOUNT_DIR_URI . 'css/admin.css','','1');
    }

    public function enqueueAdminJs(): void
    {
        wp_enqueue_script('discountAdminJs', DISCOUNT_DIR_URI . 'js/admin.js', '', '1', true);
    }
}