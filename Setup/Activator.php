<?php


namespace DiscountManager\Setup;


class Activator
{
    private $db;
    /**
     * @var string
     */
    private $tableName;

    public function __construct(\wpdb $db)
    {
        $this->db = $db;
    }

    public function init(): void
    {
        $this->tableName = PRODUCT_DISCOUNT_TABLE_NAME;
        $this->createDiscountTable();
        $this->createIndexes();
    }

    private function createDiscountTable(): void
    {
        $charset = $this->db->get_charset_collate();

        $sql = "CREATE TABLE {$this->tableName} ( 
        `productDiscountId` BIGINT(20) NOT NULL AUTO_INCREMENT,
        `productId` BIGINT(20) NOT NULL UNIQUE,
        `dateStart` INT(12) NULL, 
        `dateEnd` INT(12) NULL DEFAULT NULL, 
        `quantityStep` INT(3) NOT NULL DEFAULT 1, 
        `quantitySold` INT(4) NOT NULL, 
        `discountPercentage` INT(3) NOT NULL, 
        `discountValue` INT(4) NOT NULL, 
        `minEndPrice` INT(3) NULL DEFAULT 0,
        `orderIds` BLOB NULL DEFAULT NULL,
        `isActive` INT (1) DEFAULT 1, 
        PRIMARY KEY (`productDiscountId`))$charset;";

        require_once(ABSPATH . "wp-admin/includes/upgrade.php");
        dbDelta($sql);
    }

    private function createIndexes(): void
    {
        $this->createIndex('quantityStep');
        $this->createIndex('quantitySold');
        $this->createIndex('isActive');
        $this->createIndex('orderIds');
    }

    private function createIndex($columnName): void
    {
        $indexName = $columnName;
        $sql = "CREATE INDEX $indexName ON $this->tableName ({$columnName}) ";
        $this->db->query($sql);
    }
}