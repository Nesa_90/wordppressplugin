<?php


namespace DiscountManager\Mapper;


class ProductDiscount
{
    /**
     * @var \wpdb
     */
    private $db;

    /**
     * ProductDiscount constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
    }

    public function insert(\DiscountManager\Model\ProductDiscount $model)
    {
        $tableName = TABLE_NAME;
        $sql = "INSERT INTO $tableName (`productId`, `dateStart`, `dateEnd`,`quantityStep`,`discountValuePercentage`,
                   `discountValueInt`, `minEndPrice`,`isActive`)";
    }
}