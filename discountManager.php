<?php
/**
 * Plugin Name: Discount Manager
 * Plugin URI:
 * Description:
 * Version: 1.0.0
 * Author: Green Friends
 * Author URI: https://greenfriends.systems.com
 * Text Domain: discountManager
 * Domain Path:
 * Requires at least: 5.1
 * Requires PHP: 7.3
 *
 * @package DiscountManager
 */

use DiscountManager\MenuPage\MenuPage;
use ProductDiscount\Service\Order;
use DiscountManager\Setup\Activator;
use DiscountManager\Setup\Setup;
use ProductDiscount\Service\SaleScheduler;

global $wpdb;
define('DISCOUNT_DIR_URI', plugin_dir_url(__DIR__ . '/discountManager/'));
define('DISCOUNT_DIR', __DIR__ . '/');
DEFINE('PRODUCT_DISCOUNT_TABLE_NAME', $wpdb->prefix.'productDiscount');
include DISCOUNT_DIR.'autoloader.php';

$activator = new Activator($wpdb);
register_activation_hook(__FILE__, [$activator,'init']);
$menuPage = new MenuPage('Discount Manager', 'Discount Manager','manage_options',
'discount-manager');
$order = new Order();
$saleScheduler = new SaleScheduler();
$setup = new Setup($menuPage, $order, $saleScheduler);
$setup->setup();

