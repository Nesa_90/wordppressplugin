<?php


namespace DiscountManager\Repository;
use \DiscountManager\Mapper\ProductDiscount as Mapper;
use DiscountManager\Model\ProductDiscount as Model;

class ProductDiscount
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * ProductDiscount constructor.
     * @param Mapper $mapper
     */
    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    public function create($data)
    {
        $this->mapper->insert($this->make($data));
    }

    private function make($data)
    {
        if ($data['productDiscountId'] !== ''){
            $productDiscountId = $data['productDiscountId'];
        }
        $productId = $data['productId'];
        $dateStart = $data['dateStart'];
        $dateEnd = $data['dateEnd'];
        $quantityStep = $data['quantityStep'];
        $discountValuePercentage = $data['discountValuePercentage'];
        $discountValue = $data['discountValueInt'];
        $minEndPrice = $data['minEndPrice'];
        return new Model($productId,$quantityStep,$minEndPrice,'', $dateStart, $dateEnd,
            $discountValuePercentage, $discountValue);
    }
}