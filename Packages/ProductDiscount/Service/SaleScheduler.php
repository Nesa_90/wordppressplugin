<?php


namespace ProductDiscount\Service;


use ProductDiscount\Repository\ProductDiscount;

class SaleScheduler
{
    /**
     * @var ProductDiscount
     */
    private $discountRepo;

    /**
     * SaleScheduler constructor.
     */
    public function __construct()
    {
        $this->discountRepo = new ProductDiscount(new \ProductDiscount\Mapper\ProductDiscount());
    }

    public function hooksAndFilters(): void
    {
        add_action('init', function () {
            add_action('productDiscountScheduleSale', [$this, 'scheduleSale'],10 ,1);
            add_action('productDiscountDisableSale', [$this, 'disableSale'],10 ,1);
        });
    }

    public function scheduleSale($discountId): void
    {
        $this->discountRepo->changeDiscountToActive($discountId);
    }

    public function disableSale($discountId): void
    {
        $this->discountRepo->changeDiscountToInactive($discountId);
    }
}