<?php


namespace ProductDiscount\Service;



class PostFormatter
{
    public static function formatDataFromForm($postData)
    {
        if (count($postData) === 0){
           wp_safe_redirect(get_admin_url('','/?page=discount-manager'));
        }
        $data = [];
        $data['quantityStep'] = (int)$postData['quantityStep'];
        $data['discountPercentage'] = (int)$postData['discountPercentage'];
        $data['discountValue'] = (int)$postData['discountValue'];
        $data['minEndPrice'] = (int)($postData['minEndPrice'] ?? 0);
        if ($postData['dateStart'] !== ''){
            $dateStart = \DateTime::createFromFormat('yy-m-d', $postData['dateStart']);
            $dateStart->setTime(0,0);
            $data['dateStart'] = $dateStart->getTimestamp();
        } else{
            $dt = new \DateTime();
            $data['dateStart'] = $dt->getTimestamp();
        }
        if ($postData['dateEnd'] !== ''){
            $dateEnd = \DateTime::createFromFormat('yy-m-d', $postData['dateEnd']);
            $dateEnd->setTime(0,0);
            $data['dateEnd'] = $dateEnd->getTimestamp();
        }
        $data['productId'] = (int)$postData['productId'];
        return $data;
    }
}