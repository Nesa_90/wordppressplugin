<?php


namespace ProductDiscount\Service;


use ProductDiscount\Repository\ProductDiscount;
use WC_Order_Item;

class Order
{
    /**
     * @var ProductDiscount
     */
    private $discountRepo;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->discountRepo = new ProductDiscount(new \ProductDiscount\Mapper\ProductDiscount());
    }

    /**
     * One place to put all filters and hooks
     */
    public function hooksAndFilters(): void
    {
        add_action('init', function () {
            add_action('woocommerce_order_status_changed', [$this, 'checkForGroupBuyProducts'], 99, 4);
        });
        add_filter('woocommerce_register_shop_order_post_statuses', [$this, 'registerCustomOrderStatuses']);
        add_filter('wc_order_statuses', [$this, 'addCustomOrderStatusesToWc'], 10, 1);
        add_filter('bulk_actions-edit-shop_order', [$this, 'addCustomOrderStatusesToBulkEdit']);
        add_filter('wc_order_is_editable', [$this, 'allowEditingOfOrder'], 10, 2);
    }

    /**
     * Registers new order statuses to wc statuses
     * Name must start with wc- and has some character limit
     *
     * @param $orderStatuses
     * @return mixed
     */
    public function registerCustomOrderStatuses($orderStatuses)
    {
        $orderStatuses['wc-group-sale-p'] = [
            'label' => 'Grupna kupovina - procesiranje',
            'public' => false,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Grupna kupovina - procesiranje <span class="count">(%s)</span>',
                'Grupna kupovina - procesiranje <span class="count">(%s)</span>', 'woocommerce'),
        ];
        $orderStatuses['wc-group-sale-e'] = [
            'label' => 'Grupna kupovina - izmena',
            'public' => false,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Grupna kupovina - izmena <span class="count">(%s)</span>',
                'Grupna kupovina - izmena <span class="count">(%s)</span>', 'woocommerce'),
        ];
        $orderStatuses['wc-group-sale-c'] = [
            'label' => 'Grupna kupovina - izvršeno',
            'public' => false,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Grupna kupovina - izvršeno <span class="count">(%s)</span>',
                'Grupna kupovina - izvršeno <span class="count">(%s)</span>', 'woocommerce'),
        ];
        return $orderStatuses;
    }

    /**
     * Adds custom order status to list in admin edit order
     *
     * @param $orderStatuses
     * @return mixed
     */
    public function addCustomOrderStatusesToWc($orderStatuses)
    {
        $orderStatuses['wc-group-sale-p'] = _x('Grupna kupovina - procesiranje', 'Order status', 'woocommerce');
        $orderStatuses['wc-group-sale-e'] = _x('Grupna kupovina - izmena', 'Order status', 'woocommerce');
        $orderStatuses['wc-group-sale-c'] = _x('Grupna kupovina - izvršeno', 'Order status', 'woocommerce');
        return $orderStatuses;
    }

    /**
     * Adds customr order status to bulk edit menu
     *
     * @param $actions
     * @return mixed
     */
    public function addCustomOrderStatusesToBulkEdit($actions)
    {
        $actions['mark_group-sale-p'] = 'Change status to Grupna kupovina - procesiranje';
        $actions['mark_group-sale-e'] = 'Change status to Grupna kupovina - izmena';
        $actions['mark_group-sale-c'] = 'Change status to Grupna kupovina - izvršeno';
        return $actions;
    }

    /**
     * Allows editing of order with our custom statuses
     *
     * @param $editable
     * @param $order
     * @return bool
     */
    public function allowEditingOfOrder($editable, $order): bool
    {
        $status = $order->get_status();
        if ($status === 'group-sale-p' || $status === 'group-sale-e') {
            $editable = true;
        }
        return $editable;
    }

    /**
     * If order status changes to group-sale-p(our custom status for processing) and is coming from statuses that are
     * not our custom status group-sale-e(our custom status for edits on order) we check if some of the order items
     * are on sale and if they are we apply business logic for discount and increase qty sold.
     *
     * @param $orderId
     * @param $oldStatus
     * @param $newStatus
     * @param $order
     */
    public function checkForGroupBuyProducts($orderId, $oldStatus, $newStatus, $order): void
    {
        if ($newStatus === "group-sale-p" && $oldStatus !== 'group-sale-e') {
            /** @var WC_Order_Item $item */
            foreach ($order->get_items() as $item) {
                $productId = $item->get_product_id();
                if ($this->checkIfItemIsOnSale($productId)) {
                    $this->bindOrderToSale($orderId, $productId);
                    $discountData = $this->getDiscountData($productId);
                    $i = $item->get_quantity();
                    /*
                     * To avoid going to db few times we increase local var for each sold product to check if price should
                     * go down and then we send sold quantity to database once.
                     */
                    $quantitySold = $discountData->getQuantitySold();
                    while ($i !== 0) {
                        if ($quantitySold === 0 && $discountData->getQuantityStep() === 1){
                            $this->reduceProductPrice($productId, $discountData->getDiscountValue(),
                                $discountData->getMinEndPrice());
                        }
                        if ($quantitySold !== 0 && $quantitySold % $discountData->getQuantityStep() === 0) {
                            $this->reduceProductPrice($productId, $discountData->getDiscountValue(),
                                $discountData->getMinEndPrice());
                        }
                        $quantitySold++;
                        $i--;
                    }
                    $this->increaseQuantitySold($productId, $item->get_quantity());
                }
            }
        }
    }

    /**
     * Reduces product price by given amount and does not go below minimum given price
     * @param $productId
     * @param $reduceAmount
     * @param $minPrice
     */
    private function reduceProductPrice(int $productId, int $reduceAmount, int $minPrice): void
    {
        $product = wc_get_product($productId);
        $productPrice = $product->get_sale_price();
        //If there is no sale price we take full price
        if ($productPrice === '') {
            $productPrice = $product->get_price();
        }
        $newPrice = (int)$productPrice - $reduceAmount;
        if ($newPrice <= $minPrice) {
            $newPrice = $minPrice;
        }
        $product->set_sale_price($newPrice);
        $product->save();
    }

    /**
     * Checks if some of the order items are detected in our productDiscount table
     *
     * @param $itemId
     * @return bool
     */
    private function checkIfItemIsOnSale($itemId): bool
    {
        $items = $this->discountRepo->getItemsIdsOnSale();
        if (in_array($itemId, $items, false)) {
            return true;
        }
        return false;
    }

    /**
     * Gets data from productDiscount table by id of the product
     *
     * @param $productId
     * @return \ProductDiscount\Model\ProductDiscount
     */
    private function getDiscountData($productId): \ProductDiscount\Model\ProductDiscount
    {
        return $this->discountRepo->getItemByProductId($productId);
    }

    /**
     * Increases quantity sold in our productDiscount table
     *
     * @param $productId
     * @param null $qty
     */
    private function increaseQuantitySold($productId, $qty = null): void
    {
        $this->discountRepo->increaseQuantitySold($productId, $qty);
    }

    /**
     * Adds order to list of orders that contains item on sale so we can process payments later for group sale buisnis
     * logic
     *
     * @param int $orderId
     * @param int $productId
     */
    private function bindOrderToSale(int $orderId, int $productId): void
    {
        $this->discountRepo->bindOrderToSale($orderId, $productId);
    }
}