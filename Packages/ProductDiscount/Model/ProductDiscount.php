<?php


namespace ProductDiscount\Model;


class ProductDiscount
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var int $productId
     */
    private $productId;
    /**
     * @var string $dateStart
     */
    private $dateStart;
    /**
     * @var string $dateEnd
     */
    private $dateEnd;
    /**
     * @var int $quantityStep
     */
    private $quantityStep;
    /**
     * @var int $quantitySold
     */
    private $quantitySold;
    /**
     * @var string $discountPercentage
     */
    private $discountPercentage;
    /**
     * @var int $discountValue
     */
    private $discountValue;
    /**
     * @var int $minEndPrice
     */
    private $minEndPrice;
    /**
     * @var int $isActive
     */
    private $isActive;

    /**
     * DiscountManager constructor.
     * @param int $productId
     * @param int $quantityStep
     * @param int $minEndPrice
     * @param int|null $quantitySold
     * @param string|null $dateStart
     * @param string|null $dateEnd
     * @param int|null $discountPercentage
     * @param int|null $discountValue
     * @param int|null $id
     * @param int $isActive
     */
    public function __construct(
        int $productId,
        int $quantityStep,
        int $minEndPrice,
        int $discountPercentage,
        int $discountValue,
        int $quantitySold = null,
        string $dateStart = null,
        string $dateEnd = null,
        int $id = null,
        int $isActive = 1
    ) {
        $this->id = $id;
        $this->productId = $productId;
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
        $this->quantityStep = $quantityStep;
        $this->quantitySold = $quantitySold;
        $this->discountPercentage = $discountPercentage;
        $this->discountValue = $discountValue;
        $this->minEndPrice = $minEndPrice;
        $this->isActive = $isActive;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getDateStart($format = null): ?string
    {
        if ($this->dateStart !== '' && $this->dateStart !== null) {
            if ($format) {
                $dt = new \DateTime();
                $dt->setTimestamp((int)$this->dateStart);
                $this->dateStart = $dt->format($format);
            }
        }
        return $this->dateStart;
    }

    /**
     * @return string
     */
    public function getDateEnd($format = null): ?string
    {
        if ($this->dateEnd !== '' && $this->dateEnd !== null) {
            if ($format) {
                $dt = new \DateTime();
                $dt->setTimestamp((int)$this->dateEnd);
                $this->dateEnd = $dt->format($format);
            }
        }
        return $this->dateEnd;
    }

    /**
     * @return int
     */
    public function getQuantityStep(): int
    {
        return $this->quantityStep;
    }

    /**
     * @return int
     */
    public function getQuantitySold(): int
    {
        return $this->quantitySold;
    }

    /**
     * @return int
     */
    public function getDiscountPercentage(): int
    {
        return $this->discountPercentage;
    }

    /**
     * @return int
     */
    public function getDiscountValue(): int
    {
        return $this->discountValue;
    }

    /**
     * @return int
     */
    public function getMinEndPrice(): int
    {
        return $this->minEndPrice;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }


}