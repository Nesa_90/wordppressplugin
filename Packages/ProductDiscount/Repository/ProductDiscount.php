<?php


namespace ProductDiscount\Repository;

use ProductDiscount\Mapper\ProductDiscount as Mapper;
use ProductDiscount\Model\ProductDiscount as Model;

class ProductDiscount
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * ProductDiscount constructor.
     * @param Mapper $mapper
     */
    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    public function create($data): ?int
    {
        return $this->mapper->insert($this->make($data));
    }

    public function getItemsIdsOnSale(): array
    {
        $items = [];
        $data = $this->mapper->getItemsIdsOnSale();
        foreach ($data as $item) {
            foreach ($item as $itemValue) {
                $items[] = $itemValue;
            }
        }
        return $items;
    }

    public function getItemByProductId($productId): Model
    {
        return $this->make($this->mapper->getItemByProductId($productId));
    }

    public function increaseQuantitySold($productId, $qty = null): void
    {
        $this->mapper->increaseQuantitySold($productId, $qty);
    }

    public function getAll(int $page, int $perPage): array
    {
        $items = [];
        $data = $this->mapper->getAll($page, $perPage);
        foreach ($data as $item) {
                $items[] = $this->make($item);
        }
        return $items;
    }

    public function getDiscountById(int $discountId): Model
    {
        return $this->make($this->mapper->getDiscountById($discountId));
    }

    public function update($data): void
    {
        $this->mapper->update($this->make($data));
    }

    private function make($data): Model
    {
        $productDiscountId = null;
        $dateStart = null;
        $dateEnd = null;
        $productId = null;
        $quantityStep = null;
        $discountPercentage = null;
        $discountValue = null;
        $minEndPrice = 0;
        $quantitySold = null;

        if (isset($data['productId'])) {
            $productId = $data['productId'];
        }
        if (isset($data['dateStart'])) {
            $dateStart = $data['dateStart'];
        }
        if (isset($data['dateEnd'])) {
            $dateEnd = $data['dateEnd'];
        }
        if (isset($data['quantityStep'])) {
            $quantityStep = $data['quantityStep'];
        }
        if (isset($data['discountPercentage'])) {
            $discountPercentage = $data['discountPercentage'];
        }
        if (isset($data['discountValue'])) {
            $discountValue = $data['discountValue'];
        }
        if (isset($data['discountValue'])) {
            $minEndPrice = $data['minEndPrice'];
        }
        if (isset($data['quantitySold'])) {
            $quantitySold = $data['quantitySold'];
        }
        if ($quantitySold === '') {
            $quantitySold = null;
        }
        if (isset($data['productDiscountId'])){
            $productDiscountId = $data['productDiscountId'];
        }

        return new Model($productId, $quantityStep, $minEndPrice, $discountPercentage, $discountValue,
                $quantitySold, $dateStart, $dateEnd, $productDiscountId);
    }

    public function bindOrderToSale(int $orderId, int $productId): void
    {
        $this->mapper->bindOrderToSale($orderId, $productId);
    }

    public function delete(int $discountId): void
    {
        $this->mapper->delete($discountId);
    }

    public function getByOrderId(int $orderId): array
    {
        $items = [];
        $data = $this->mapper->getItemsByOrderId($orderId);
        foreach ($data as $item) {
            $items[] = $this->make($item);
        }
        return $items;
    }

    public function changeDiscountToActive(int $discountId): void
    {
        $this->mapper->changeDiscountToActive($discountId);
    }

    public function changeDiscountToInactive($discountId)
    {
        $this->mapper->changeDiscountToInactive($discountId);
    }
}