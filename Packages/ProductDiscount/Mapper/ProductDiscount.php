<?php


namespace ProductDiscount\Mapper;


class ProductDiscount
{
    /**
     * @var \wpdb
     */
    private $db;
    /**
     * @var string
     */
    private $tableName;

    /**
     * ProductDiscount constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
        $this->tableName = PRODUCT_DISCOUNT_TABLE_NAME;
    }

    public function insert(\ProductDiscount\Model\ProductDiscount $model): ?int
    {
        $this->db->insert($this->tableName,[
            'productId' => $model->getProductId(),
            'dateStart' => $model->getDateStart(),
            'dateEnd' => $model->getDateEnd(),
            'quantityStep' => $model->getQuantityStep(),
            'discountPercentage' => $model->getDiscountPercentage(),
            'discountValue' => $model->getDiscountValue(),
            'minEndPrice' => $model->getMinEndPrice()
        ],['%d', '%s', '%s', '%d', '%d', '%d', '%d']);
        return $this->db->insert_id;
    }

    public function getItemsIdsOnSale(): array
    {
        $sql = "SELECT `productId` FROM $this->tableName WHERE `isActive` = 1;";
        return $this->db->get_results($sql,ARRAY_N);
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getItemByProductId($productId)
    {
        $sql = "SELECT * FROM $this->tableName WHERE `productId` = $productId";
        return $this->db->get_results($sql, ARRAY_A)[0];
    }

    public function increaseQuantitySold($productId, $quantity = null): void
    {
        $qty = 1;
        if ($quantity){
            $qty = $quantity;
        }
        $sql = "UPDATE $this->tableName SET `quantitySold` = `quantitySold` + $qty WHERE `productID` = $productId";
        $this->db->query($sql);
    }

    public function getAll(int $page, int $perPage)
    {
        $limit = $perPage;
        $offset = 0;
        if ($page !== 1) {
            $offset = $page * $limit;
        }
        $sql = "SELECT * FROM $this->tableName LIMIT $limit OFFSET $offset;";
        return $this->db->get_results($sql, ARRAY_A);
    }

    public function getDiscountById(int $discountId)
    {
        $sql = "SELECT * FROM $this->tableName WHERE `productDiscountId` = $discountId;";
        return $this->db->get_results($sql, ARRAY_A)[0];
    }

    public function update(\ProductDiscount\Model\ProductDiscount $model): void
    {
        $this->db->update($this->tableName,
            [
                'dateStart' => $model->getDateStart(),
                'dateEnd' => $model->getDateEnd(),
                'quantityStep' => $model->getQuantityStep(),
                'discountPercentage' => $model->getDiscountPercentage(),
                'discountValue' => $model->getDiscountValue(),
                'minEndPrice' => $model->getMinEndPrice()
            ],['productDiscountId' => $model->getId()],['%s', '%s', '%d', '%d', '%d', '%d']);
    }

    public function bindOrderToSale(int $orderId, int $productId): void
    {
        $sql = "UPDATE $this->tableName SET orderIds = CONCAT_WS(',',orderIds, $orderId) WHERE productId = $productId;";
        $this->db->query($sql);
    }

    public function delete(int $discountId): void
    {
        $sql = "DELETE FROM $this->tableName WHERE `productDiscountId` = $discountId";
        $this->db->query($sql);
    }

    public function getItemsByOrderId(int $orderId): ?array
    {
        $sql = "SELECT * FROM $this->tableName WHERE orderIds LIKE '%{$orderId}%'";
        return $this->db->get_results($sql, ARRAY_A);
    }

    public function changeDiscountToActive(int $discountId): void
    {
        $this->db->update($this->tableName,
            [
                'isActive' => 1
            ],['productDiscountId' => $discountId],['%d']);
    }

    public function changeDiscountToInactive($discountId)
    {
        $this->db->update($this->tableName,
            [
                'isActive' => 0
            ],['productDiscountId' => $discountId],['%d']);
    }
}