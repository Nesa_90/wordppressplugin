<?php

require DISCOUNT_DIR . '/MenuPage/MenuPage.php';
require DISCOUNT_DIR . '/Setup/Activator.php';
require DISCOUNT_DIR . '/Setup/Setup.php';

$dir = new RecursiveDirectoryIterator(DISCOUNT_DIR . '/Packages');
foreach (new RecursiveIteratorIterator($dir) as $file) {
    if (!is_dir($file)) {
        if (fnmatch('*.php', $file)) {
            require $file;
        }
    }
}